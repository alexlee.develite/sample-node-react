import express, { Router, Request, Response } from "express";
// import api from "./api";
import Logger from './loaders/logger';
import routes from './api';
import config from './config';
import cors from 'cors';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

const port = process.env.SERVER_PORT; // default port to listen
const app = express();

// connect to database
mongoose.connect('mongodb://localhost:27017/rppdb', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => {
    Logger.info(`
        ###############################################
        ###### Connected to mongodb at: 27017 ######
        ###############################################
        `);
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(config.api.prefix, routes());

app.listen(port, () => {
    Logger.info(`
        ###############################################
        ###### Server listening on port: ${port} ######
        ###############################################
        `);
}).on('error', err => {
    Logger.error(err);
    process.exit(1);
});