import { triggerAsyncId } from 'async_hooks';
import mongoose from 'mongoose';

const User = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    }
});

export default mongoose.model<IUser & mongoose.Document>('User', User);
