import User from "./../models/user";


export default class UserService {

    public async getAllUsers() {
        return await User.find({});
    }
}