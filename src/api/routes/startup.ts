import { Router, Request, Response } from 'express';

const route = Router();

export default (app: Router) => {
    app.use('/startup', route);

    route.get('/ping', (req: Request, res: Response) => {
        return res.send('Ping');
    });
}