import { Router, Request, Response } from 'express';
import UserService from './../../services/userService';
import User from './../../models/user';
import { loggers } from 'winston';
import Logger from './../../loaders/logger';

const route = Router();

export default (app: Router) => {
    app.use('/users', route);

    route.get('/', [], async (req: Request, res: Response) => {
        const users = await User.find({});
        return res.status(200).send(users);
    });

    route.post('/', async (req: Request, res: Response) => {
        Logger.info("Request", req);
        const { name, email } = req.body;

        const user = new User({ name, email });
        await user.save()
            .then(() => {
                return res.status(204);
            })
            .catch((error: any) => {
                Logger.error('error:', error);
                alert('error creating user');
            });

        return res.json().status(200);
    });

    route.get('/:userId', async (req: Request, res: Response) => {
        const userId = req.params.userId;
        const user = await User.find({_id : userId});
        return res.status(200).send(user);
    });

    route.delete('/:userId', async (req: Request, res: Response) => {
        Logger.info("Deleting: " + req.params.userId);
        const userId = req.params.userId;
        await User.remove({ _id: userId });

        return res.json().status(200);
    });

    route.post('/:userId', async (req: Request, res: Response) => {
        Logger.info("Updating: " + req.params.userId);
        const userId = req.params.userId;
        const { name, email } = req.body;
        User.findOneAndUpdate({ _id: userId }, { name, email }, (err: any, place: any) => {
            Logger.error("Error updating " + userId)
        });
        return res.json().status(200);
    });
}