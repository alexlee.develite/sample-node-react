import { Router } from 'express';
import startup from './routes/startup';
import users from './routes/users';


export default () => {
	const router = Router();
	startup(router);
    users(router);
	return router
}