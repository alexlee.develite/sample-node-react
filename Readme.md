npm init
npm install typescript express ts-node


Run mongodb
./mongod --dbpath "D:/sandbox/mongodb" --logpath "D:/sandbox/mongodb/logs.txt" --install --serviceName "MongoDB"


To create a mongdodb user
db.createUser(
{
	user: "rppadmin",

	pwd: "p@ssw0rd",

	roles:[{role: "userAdmin" , db:"rppdb"}]});